package com.nikolaenko.andrew.mobileservice

import android.app.Application
import android.content.ContextWrapper
import com.github.salomonbrys.kodein.*
import com.nikolaenko.andrew.mobileservice.network.APIPublicService
import com.nikolaenko.andrew.mobileservice.network.RetrofitFactory
import com.pixplicity.easyprefs.library.Prefs
import org.jetbrains.anko.ctx

class App :  Application(), KodeinAware  {

    override val kodein by Kodein.lazy {
        bind<APIPublicService>() with provider { RetrofitFactory.getForLogin(ctx.getString(R.string.api_base_url)).create(APIPublicService::class.java) }
    }

    override fun onCreate() {
        super.onCreate()

        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()

    }
}