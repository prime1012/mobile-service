package com.nikolaenko.andrew.mobileservice.fragment.client

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_about_service.*

class FragmentAboutService : BaseFragment() {

    private var logoImg: String? = null
    private var titleSer: String? = null
    private var typeServ: String? = null
    private var distanceServ: String? = null

    override fun onStart() {
        super.onStart()

        logoImg = arguments?.getString(ARG_LOGO)
        titleSer = arguments?.getString(ARG_TITLE)
        typeServ = arguments?.getString(ARG_TYPE)
        distanceServ = arguments?.getString(ARG_DISTANCE)


        if (logoImg == "images/no_photo.jpg" || logoImg == "imgBase64"){
            Glide.with(this).load(R.drawable.meme_determined).into(logo)
        } else {

            val url = getString(R.string.api_base_url) + logoImg + ".png"
            Glide.with(this).load(url).into(logo)

//            val decodedString = Base64.decode("images/$logoImg", Base64.URL_SAFE)
//            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
//            Glide.with(this).asBitmap().load(decodedByte).into(logo)
        }


        title.text = titleSer
        type.text = typeServ
        distance.text = distanceServ

        toolbarTitle.text = titleSer

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }

    }

    companion object {

        private val ARG_LOGO = "arg_logo"
        private val ARG_TITLE = "arg_title"
        private val ARG_TYPE = "arg_type"
        private val ARG_DISTANCE = "arg_distance"

        fun newInstance(logo: String, title: String, type: String, distance: String): FragmentAboutService {

            val args = Bundle()
            args.putString(ARG_LOGO, logo)
            args.putString(ARG_TITLE, title)
            args.putString(ARG_TYPE, type)
            args.putString(ARG_DISTANCE, distance)

            val fragment = FragmentAboutService()
            fragment.arguments = args
            return fragment
        }
    }
}