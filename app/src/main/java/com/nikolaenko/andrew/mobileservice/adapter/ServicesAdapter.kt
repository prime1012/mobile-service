package com.nikolaenko.andrew.mobileservice.adapter

import android.support.v7.widget.RecyclerView
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.model.ServicesModel
import org.jetbrains.anko.find
import android.graphics.BitmapFactory
import android.graphics.Bitmap



class ServicesAdapter (private val callbackDetails: ((logo: String, title: String, type: String, distance: String) -> Unit)? = null,
                       private val callbackChoose: ((id: Int) -> Unit)? = null) : RecyclerView.Adapter<ServicesAdapter.MyViewHolder>() {

    private var list: MutableList<ServicesModel> = ArrayList()

    fun setList(list: MutableList<ServicesModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imageLogo: ImageView = view.find(R.id.logo)
        var tvTitle: TextView = view.find(R.id.title)
        var tvType: TextView = view.find(R.id.type)
        var tvDistance: TextView = view.find(R.id.distance)

        var tvDetails: TextView = view.find(R.id.details)
        var tvChoose: TextView = view.find(R.id.choose)

        var item = view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_for_service_list, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]

        if (item.logo == "images/no_photo.jpg" || item.logo == "imgBase64"){
            Glide.with(holder.itemView.context).load(R.drawable.meme_determined).into(holder.imageLogo)
        } else {

            val url = holder.item.context.getString(R.string.api_base_url) + item.logo + ".png"
            Glide.with(holder.item.context).load(url).into(holder.imageLogo)
        }

        holder.tvTitle.text  = item.title
        holder.tvType.text = item.services
        holder.tvDistance.text = item.address

        holder.tvDetails.setOnClickListener {
            callbackDetails?.invoke(item.logo.toString(), item.title.toString(), item.services.toString(), item.address.toString())
        }

        holder.tvChoose.setOnClickListener {
            callbackChoose?.invoke(item.id!!)
        }

    }

    override fun getItemCount(): Int = list.size
}
