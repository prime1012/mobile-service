package com.nikolaenko.andrew.mobileservice.model

import com.google.gson.annotations.SerializedName

class OrderModelNew {

    @SerializedName("createdAt")
    var createdAt: Long? = null

    @SerializedName("updatedAt")
    var updatedAt: Long? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("driverId")
    var driverId: Int? = null

    @SerializedName("customerId")
    var customerId: Int? = null

    @SerializedName("serviceId")
    var serviceId: Int? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("repairType")
    var repairType: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("deviceType")
    var deviceType: String? = null

    @SerializedName("brand")
    var brand: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("shipmentDate")
    var shipmentDate: String? = null

    @SerializedName("photo")
    var photo: String? = null

    @SerializedName("feedback")
    var feedback: String? = null
}