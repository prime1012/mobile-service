package com.nikolaenko.andrew.mobileservice.fragment.service

import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.adapter.ActivityLinePagerAdapter
import com.nikolaenko.andrew.mobileservice.adapter.OrdersAdapter
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.*
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.service_prising_fragment.*
import org.jetbrains.anko.support.v4.toast
import java.util.HashMap

class FragmentPrices : BaseFragment() {

    private var infoDisposable: Disposable? = null
    private var id: Int? = null

    override fun onStart() {
        super.onStart()
        id = Prefs.getInt("service_id", 0)
        getServiceInfo(id!!)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.service_prising_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        applyScreen.setOnClickListener {
            if (screenValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, screen.text.toString(), screenValue.text.toString())
            }
        }

        applyBaterry.setOnClickListener {
            if (baterryValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, baterry.text.toString(), baterryValue.text.toString())
            }
        }

        applyDiagnostic.setOnClickListener {
            if (diagnosticValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, diagnostic.text.toString(), diagnosticValue.text.toString())
            }
        }

        applyDamage.setOnClickListener {
            if (damageValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, damage.text.toString(), damageValue.text.toString())
            }
        }

        applyWaterDamage.setOnClickListener {
            if (waterDamageValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, waterDamage.text.toString(), waterDamageValue.text.toString())
            }
        }

        applyDataRepeir.setOnClickListener {
            if (dataRepeirValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, dataRepeir.text.toString(), dataRepeirValue.text.toString())
            }
        }

        applyCorpus.setOnClickListener {
            if (corpusValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, corpus.text.toString(), corpusValue.text.toString())
            }
        }

        applyTemperature.setOnClickListener {
            if (temperatureValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, temperature.text.toString(), temperatureValue.text.toString())
            }
        }

        applyPo.setOnClickListener {
            if (poValue.text.isEmpty()){
                toast("Введите сначала цену!!!")
                return@setOnClickListener
            } else {
                changeCoast(id!!, po.text.toString(), poValue.text.toString())
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }

    }

    private fun getServiceInfo(serviceId: Int){

        infoDisposable = apiPulicService.value.getServiceInfo(serviceId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: ServiceResponse ->

                    if (t != null){

                        if (t.services!!.contains("Замена экрана")){
                            screenLayout.visibility = View.VISIBLE
                        }

                        if ( t.services!!.contains("Замена батареи")){
                            baterryLayout.visibility = View.VISIBLE
                        }

                        if ( t.services!!.contains("Диагностика")){
                            diagnosticLayout.visibility = View.VISIBLE
                        }
                        if ( t.services!!.contains("Случайные повреждения")){
                            damageLayout.visibility = View.VISIBLE
                        }
                        if ( t.services!!.contains("Повреждения от воды")){
                            waterDamageLayout.visibility = View.VISIBLE
                        }
                        if ( t.services!!.contains("Восстановление данных")){
                            dataRepeirLayout.visibility = View.VISIBLE
                        }
                        if ( t.services!!.contains("Проблемма с корпусом")){
                            corpusLayout.visibility = View.VISIBLE
                        }
                        if ( t.services!!.contains("Проблемма с перегревом")){
                            temperatureLayout.visibility = View.VISIBLE
                        }
                        if ( t.services!!.contains("Проблемма с ПО")){
                            poLayout.visibility = View.VISIBLE
                        }
                    }

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })

    }

    private fun changeCoast(id: Int, title: String, amount: String){

        showProgress(true)

        infoDisposable = apiPulicService.value.updateCoast(id, title, amount.toInt())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: PriceModel ->

                    goBack()

                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }
}