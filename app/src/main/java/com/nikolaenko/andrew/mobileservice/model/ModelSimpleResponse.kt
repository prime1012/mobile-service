package com.nikolaenko.andrew.mobileservice.model

import com.google.gson.annotations.SerializedName

class ModelSimpleResponse<T> {
    
    @SerializedName("data")
    var data: T? = null

    @SerializedName("status")
    var code: Int? = null
}
