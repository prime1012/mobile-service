package com.nikolaenko.andrew.mobileservice.fragment.client

import android.os.Bundle
import android.view.*
import com.nikolaenko.andrew.mobileservice.fragment.FragmentOrders
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_home_screen.*
import org.jetbrains.anko.support.v4.toast

class FragmentHomeScreen : BaseFragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home_screen, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvStart.setOnClickListener {
            replaceFragment(FragmentStartRepair(), true)
        }

        tvSettings.setOnClickListener {
            replaceFragment(FragmentSettingsScreen(), true)
        }

        tvFaq.setOnClickListener {
            toast("Screen with text")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.client_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.myProfile -> {
                replaceFragment(FragmentMyProfile(), true)
                return true
            }
            R.id.myOrders -> {
                replaceFragment(FragmentOrders.newInstance("my_orders"), true)
                return true
            }
        }
        return false
    }
}