package com.nikolaenko.andrew.mobileservice.model

import com.google.gson.annotations.SerializedName

class PriceModel {

    @SerializedName("createdAt")
    var createdAt: Long? = null

    @SerializedName("updatedAt")
    var updatedAt: Long? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("serviceId")
    var serviceId: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("price")
    var price: Int? = null
}