package com.nikolaenko.andrew.mobileservice.fragment.client


import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.CustomerResponse
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_my_profile.*
import org.jetbrains.anko.support.v4.toast
import java.util.HashMap

class FragmentMyProfile : BaseFragment() {

    private var infoDisposable: Disposable? = null
    private var changes: HashMap<String, Any?>? = null
    override fun onStart() {
        super.onStart()
        getUserInfo()
        changes = HashMap()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvContinue.setOnClickListener {
            updateInfo()
        }
    }

    private fun getUserInfo(){
        showProgress(true)

        val myId = Prefs.getInt("customer_id", 0)
        infoDisposable = apiPulicService.value.getUserInfo(myId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: CustomerResponse ->

                    t.name.let { editFirstName.setText(it) }
                    t.surname.let { editLastName.setText(it) }
                    t.phone.let { editSecondPhone.setText(it) }
                    t.email.let { editEmail.setText(it) }
                    t.password.let { editPassword.setText(it) }

                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)

                })
    }

    private fun updateInfo(){

        showProgress(true)

        changes?.put("id", Prefs.getInt("customer_id", 0))
        changes?.put("name", editFirstName.text.toString())
        changes?.put("surname", editLastName.text.toString())
        changes?.put("phone", editSecondPhone.text.toString())
        changes?.put("email", editEmail.text.toString())
        changes?.put("password", editPassword.text.toString())

        infoDisposable = apiPulicService.value.updateUserInfo(changes)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: Any ->

                    mainActivity.hardReplaceFragment(FragmentHomeScreen())
                    showProgress(false)
                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)

                })
    }
}
