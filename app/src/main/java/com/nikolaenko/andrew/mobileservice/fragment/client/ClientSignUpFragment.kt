package com.nikolaenko.andrew.mobileservice.fragment.client


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.CustomerResponse
import com.pixplicity.easyprefs.library.Prefs
import com.redmadrobot.inputmask.MaskedTextChangedListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.client_sign_up_fragment.*
import org.jetbrains.anko.support.v4.toast
import java.util.*


class ClientSignUpFragment : BaseFragment() {

    private var regDisposable: Disposable? = null
    internal var phoneNumber: String? = null
    internal var phoneNumberFilled: Boolean = false

    private var changes: HashMap<String, Any?>? = null

    override fun onStart() {
        super.onStart()

        changes = HashMap()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.client_sign_up_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftButton.setOnClickListener {
            goBack()
        }

        editFirstName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutSurname.visibility = View.VISIBLE
                } else {
                    layoutSurname.visibility = View.GONE
                }
            }

        })

        editLastName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutPhone.visibility = View.VISIBLE
                } else {
                    layoutPhone.visibility = View.GONE
                }
            }

        })

        editPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutSecondPhone.visibility = View.VISIBLE
                    layoutEmail.visibility = View.VISIBLE
                } else {
                    layoutSecondPhone.visibility = View.GONE
                    layoutEmail.visibility = View.GONE
                }
            }

        })

        editEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutPassword.visibility = View.VISIBLE
                    layoutSecondPassword.visibility = View.VISIBLE
                } else {
                    layoutPassword.visibility = View.GONE
                    layoutSecondPassword.visibility = View.GONE
                }
            }

        })

        editPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {

            }

        })

        editSecondPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    continueBtn.visibility = View.VISIBLE
                } else {
                    continueBtn.visibility = View.GONE
                }
            }

        })

        val listener = MaskedTextChangedListener(
                "373 [000]-[00]-[00]",
                false,
                editPhone,
                null,
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(maskFilled: Boolean, extractedValue: String) {
                        phoneNumber = extractedValue
                        phoneNumberFilled = maskFilled
                    }
                }
        )

        editPhone.addTextChangedListener(listener)
        editPhone.onFocusChangeListener = listener
        editPhone.hint = listener.placeholder()

        continueBtn.setOnClickListener {

            if (editPassword.text.toString() != editSecondPassword.text.toString()){
                toast("Пароли не совпадают")
                return@setOnClickListener
            }

            showProgress(true)

            changes?.put("name", editFirstName.text.toString())
            changes?.put("surname", editLastName.text.toString())

            val phNumb = editPhone.text.toString()
            val phoneNumb = phNumb.replace(" ", "")
            val phoneNumb2 = phoneNumb.replace("-", "")

            changes?.put("phone", phoneNumb2)
            changes?.put("phoneAdditional", editSecondPhone.text.toString())
            changes?.put("email", editEmail.text.toString())
            changes?.put("password", editPassword.text.toString())

            regDisposable = apiPulicService.value.customerSignUp(changes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: CustomerResponse ->

                        if (t != null){
                            if (t.email != null && t.password != null) {


                                apiPulicService.value.customerSignIn(t.email!!, t.password!!)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({ t: CustomerResponse ->

                                            Prefs.putInt("customer_id", t.id!!)

                                            mainActivity.hardReplaceFragment(FragmentHomeScreen())
                                            showProgress(false)

                                        }, { e ->
                                            toast(e.message.toString())
                                            e.printStackTrace()
                                            showProgress(false)

                                        })

                            }
                        }

                    }, { e ->
                        toast(e.message.toString())
                        e.printStackTrace()
                        showProgress(false)
                    })
        }

    }

    override fun onStop() {
        super.onStop()
        regDisposable?.dispose()
    }
}
