package com.nikolaenko.andrew.mobileservice.activity

import android.content.ContextWrapper
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.widget.FrameLayout
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.FragmentMain
import com.nikolaenko.andrew.mobileservice.network.APIPublicService
import com.pixplicity.easyprefs.library.Prefs
import org.jetbrains.anko.toast


class MainActivity : KodeinAppCompatActivity() {

    protected val kodein = LazyKodein(appKodein)
    protected val apiPulicService = kodein.instance<APIPublicService>()

    var progressView: FrameLayout? = null

    internal var doubleBackToExitPressedOnce = false

    override fun onStart() {
        super.onStart()

        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressView = findViewById(R.id.progressMain)

        if (savedInstanceState == null){
            val fragment = FragmentMain()
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.placeholder, fragment, "main_fragment")
            ft.commit()
        }
    }

    fun hardReplaceFragment(fragment: Fragment) {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        supportFragmentManager.beginTransaction()
                .replace(R.id.placeholder, fragment)
                .commit()
    }


    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount != 0) {
            super.onBackPressed()
            return
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finish()
            return
        }

        this.doubleBackToExitPressedOnce = true
        toast("Double press BACK to exit")
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

//    private fun getCurrentLocation(){
//
//        Dexter.withActivity(mainActivity)
//                .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
//                        Manifest.permission.ACCESS_FINE_LOCATION,
//                        Manifest.permission.READ_EXTERNAL_STORAGE,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                        Manifest.permission.CAMERA,
//                        Manifest.permission.RECORD_AUDIO)
//                .withListener(object : MultiplePermissionsListener {
//
//                    @SuppressLint("MissingPermission")
//                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
//                        val gps = GPSTracker(context)
//                        if (gps.canGetLocation()) {
//                            val lat = gps.latitude
//                            val lng = gps.longitude
//
//                            Log.e("loca: ", "$lat $lng")
//
//                            if (lat != null && lng != null){
//
//                                yammyService.value.updateMyLocationObservable(lat, lng)
//                                        .subscribeOn(Schedulers.newThread())
//                                        .observeOn(AndroidSchedulers.mainThread())
//                                        .subscribe({ t: ModelSimpleResponse<Boolean> ->
//
//                                            Log.e("update location", "success")
//                                            Log.e("sended: ", "$lat $lng")
//
//                                        }, { e ->
//                                            e.printStackTrace()
//                                            showProgress(false)
//                                        })
//                            }
//
//                        } else {
//                            gps.showSettingsAlert()
//                        }
//
//
//                    }
//
//                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
//                        token!!.continuePermissionRequest()
//                    }
//
//                }).check()
//
//
//    }

}
