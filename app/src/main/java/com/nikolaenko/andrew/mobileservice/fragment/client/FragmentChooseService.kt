package com.nikolaenko.andrew.mobileservice.fragment.client


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.adapter.ServicesAdapter
import com.nikolaenko.andrew.mobileservice.model.ServicesModel
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.choose_service_layout.*
import org.jetbrains.anko.support.v4.toast
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets
import java.util.HashMap

class FragmentChooseService : BaseFragment() {

    private lateinit var adapter: ServicesAdapter
    private var servicesDisposable: Disposable? = null

    private var myId: Int? = null
    private var servId: Int? = null
    private var repairType: String? = null
    private var description: String? = null
    private var deviceType: String? = null
    private var brand: String? = null
    private var address: String? = null
    private var shipmentDate: String? = null
    private var photo: String? = null

    private var changes: HashMap<String, Any?>? = null

    override fun onStart() {
        super.onStart()
        changes = HashMap()
        getServicesList()

        myId = arguments?.getInt(ARG_MY_ID)!!
        repairType = arguments?.getString(ARG_REP_TYPE)!!
        description = arguments?.getString(ARG_DESCRIPTION)!!
        deviceType = arguments?.getString(ARG_DEV_TYPE)!!
        brand = arguments?.getString(ARG_BRAND)!!
        address = arguments?.getString(ARG_ADDRESS)!!
        photo = arguments?.getString(ARG_PHOTO)!!
        shipmentDate = arguments?.getString(ARG_TIME)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.choose_service_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvServices.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvServices.itemAnimator = DefaultItemAnimator()

        adapter = ServicesAdapter({ logo, title, type, distance ->

            replaceFragment(FragmentAboutService.newInstance(logo, title, type, distance), true)

        }, { id ->

            changes?.put("driverId", 0)
            changes?.put("customerId", myId)
            changes?.put("serviceId", id)
            changes?.put("repairType", repairType)
            changes?.put("description", description)
            changes?.put("deviceType", deviceType)
            changes?.put("brand", brand)
            changes?.put("address", address)
            changes?.put("shipmentDate", shipmentDate)
            changes?.put("photo", photo)

            servicesDisposable = apiPulicService.value.createOrder(changes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: List<OrderModelNew> ->
                        Log.e("sss", t[0].photo)
                        toast("Ваш заказ отправлен!")
                        mainActivity.hardReplaceFragment(FragmentHomeScreen())

                    }, { e ->
                        toast(e.message.toString())
                        e.printStackTrace()
                        showProgress(false)
                    })

        })

        rvServices.adapter = adapter
    }

    private fun getServicesList(){
        showProgress(true)
        servicesDisposable = apiPulicService.value.getAllServices()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<ServicesModel> ->

                    if (t !=null ){
                        adapter.setList(t.toMutableList())
                    }

                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }
    }

    companion object {

        private val ARG_MY_ID = "arg_user_id"
        private val ARG_REP_TYPE = "arg_repType"
        private val ARG_DESCRIPTION = "arg_desc"
        private val ARG_DEV_TYPE = "arg_devType"
        private val ARG_BRAND = "arg_brand"
        private val ARG_ADDRESS = "arg_address"
        private val ARG_PHOTO = "arg_photo"
        private val ARG_TIME = "arg_time"

        fun newInstance(myId: Int, repType: String, desc: String, devType: String, brand: String, address: String, photo: String, time: String): FragmentChooseService {

            val args = Bundle()
            args.putInt(ARG_MY_ID, myId)
            args.putString(ARG_REP_TYPE, repType)
            args.putString(ARG_DESCRIPTION, desc)
            args.putString(ARG_DEV_TYPE, devType)
            args.putString(ARG_BRAND, brand)
            args.putString(ARG_ADDRESS, address)
            args.putString(ARG_PHOTO, photo)
            args.putString(ARG_TIME, time)

            val fragment = FragmentChooseService()
            fragment.arguments = args
            return fragment
        }
    }

}
