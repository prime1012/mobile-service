package com.nikolaenko.andrew.mobileservice.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.activity.MainActivity
import com.nikolaenko.andrew.mobileservice.model.CustomerResponse
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import com.nikolaenko.andrew.mobileservice.model.PriceModel
import com.nikolaenko.andrew.mobileservice.model.ServiceResponse
import com.nikolaenko.andrew.mobileservice.network.APIPublicService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.zipWith
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.find
import org.jetbrains.anko.toast


class DriverOrdersAdapter(private val callbackAbout: ((id: Int, serviceId: Int, clientId: Int) -> Unit)? = null,
                          private val callbackConfirm: ((id: Int, position: Int) -> Unit)? = null): RecyclerView.Adapter<DriverOrdersAdapter.MyViewHolder>() {

    private var list: MutableList<OrderModelNew> = ArrayList()
    private var isForMy: Boolean = false
    private var srvName: String? = null
    private var srvAddress: String? = null
    private var clntName: String? = null

    fun setList(list: MutableList<OrderModelNew>, isForMy: Boolean) {
        this.list = list
        this.isForMy = isForMy
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var serviceTitle: TextView = view.find(R.id.serviceTitle)
        var serviceAddress: TextView = view.find(R.id.serviceAddress)
        var clientName: TextView = view.find(R.id.clientName)
        var clientAddress: TextView = view.find(R.id.clientAddress)

        var detailsBtn: TextView = view.find(R.id.detailsBtn)
        var applyBtn: TextView = view.find(R.id.applyBtn)

        var item = view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_for_driver_orders, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]

        val kodein = LazyKodein(holder.item.context.appKodein)
        val apiPulicService = kodein.instance<APIPublicService>()

        apiPulicService.value.getServiceInfo(item.serviceId!!)
                .zipWith(apiPulicService.value.getUserInfo(item.customerId!!)) { t1, t2 ->
                    Pair(t1, t2)
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: Pair<ServiceResponse, CustomerResponse> ->

                    srvName = t.first.title!!
                    srvAddress = t.first.address

                    clntName = t.second.name + " " + t.second.surname

                    holder.serviceTitle.text = srvName
                    holder.serviceAddress.text = srvAddress
                    holder.clientName.text = clntName
                    holder.clientAddress.text = item.address

                }, { e ->
                    e.printStackTrace()
                })


        if (isForMy){
            holder.applyBtn.visibility = View.GONE
        } else {
            holder.applyBtn.visibility = View.VISIBLE
        }

        holder.detailsBtn.setOnClickListener {
            callbackAbout?.invoke(item.id!!, item.serviceId!!, item.customerId!!)
        }
        holder.applyBtn.setOnClickListener {
            callbackConfirm?.invoke(item.id!!, position)
        }
    }

    override fun getItemCount(): Int = list.size
}
