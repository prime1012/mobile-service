package com.nikolaenko.andrew.mobileservice.model

import com.google.gson.annotations.SerializedName
import java.util.*

class DriverResponse {

    @SerializedName("createdAt")
    var createdAt: Long? = null

    @SerializedName("updatedAt")
    var updatedAt: Long? = null

    @SerializedName("id")
    var id: Int? = 0

    @SerializedName("name")
    var name: String? = null

    @SerializedName("surname")
    var surname: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("phoneAdditional")
    var phoneAdditional: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("password")
    var password: String? = null

    @SerializedName("wishesToWork")
    var wishesToWork: String? = null

    @SerializedName("isAdult")
    var isAdult: Boolean? = false

    @SerializedName("isAlowedToDrive")
    var isAlowedToDrive: Boolean? = false

    @SerializedName("licenseId")
    var licenseId: String? = null

    @SerializedName("vehicle")
    var vehicle: String? = null

    @SerializedName("hasViolation")
    var hasViolation: Boolean? = false

    @SerializedName("previousWork")
    var previousWork: String? = null
}