package com.nikolaenko.andrew.mobileservice.fragment.service

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.*
import com.bumptech.glide.Glide
import com.nikolaenko.andrew.mobileservice.fragment.FragmentOrders
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.ServiceResponse
import com.nikolaenko.andrew.mobileservice.model.ServicesModel
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home_edit_service.*
import org.jetbrains.anko.support.v4.toast
import java.util.HashMap

class FragmentSeviceEdit : BaseFragment() {

    private var infoDisposable: Disposable? = null
    var myTagList: ArrayList<String> = arrayListOf()
    private var changes: HashMap<String, Any?>? = null

    override fun onStart() {
        super.onStart()
        changes = HashMap()
        getServiceInfo(Prefs.getInt("service_id", 0))

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home_edit_service, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        applyButton.setOnClickListener {
            updateInfo()
        }
    }

    private fun getServiceInfo(serviceId: Int){

        infoDisposable = apiPulicService.value.getServiceInfo(serviceId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: ServiceResponse ->

                    if (t != null){
                        t.title?.let {  toolbarTitle.text = it }


                        if (t.logo == "images/no_photo.jpg" || t.logo == "imgBase64"){
                            Glide.with(this).load(R.drawable.meme_determined).into(logo)
                        } else {

                            val url = getString(R.string.api_base_url) + t.logo + ".png"
                            Glide.with(this).load(url).into(logo)
                        }


//                        if (t.logo == "images/no_photo.jpg" || t.logo == "imgBase64"){
//                            Glide.with(this).load(R.drawable.meme_determined).into(logo)
//                        } else {
//
//                            val decodedString = Base64.decode(t.logo, Base64.URL_SAFE)
//                            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
//                            Glide.with(this).asBitmap().load(decodedByte).into(logo)
//                        }

                        t.title?.let { title.setText(it) }
                        t.address?.let { address.setText(it) }

                        t.description?.let { aboutService.setText(it)}

                        if (t.services!!.contains("Замена экрана")){
                            screen.isChecked = true
                        }

                        if ( t.services!!.contains("Замена батареи")){
                            baterry.isChecked = true
                        }

                        if ( t.services!!.contains("Диагностика")){
                            diagnostic.isChecked = true
                        }
                        if ( t.services!!.contains("Случайные повреждения")){
                            damage.isChecked = true
                        }
                        if ( t.services!!.contains("Повреждения от воды")){
                            waterDamage.isChecked = true
                        }
                        if ( t.services!!.contains("Восстановление данных")){
                            dataRepeir.isChecked = true
                        }
                        if ( t.services!!.contains("Проблемма с корпусом")){
                            corpus.isChecked = true
                        }
                        if ( t.services!!.contains("Проблемма с перегревом")){
                            temperature.isChecked = true
                        }
                        if ( t.services!!.contains("Проблемма с ПО")){
                            po.isChecked = true
                        }
                    }

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })

    }

    private fun updateInfo(){

        showProgress(true)
        val myTagList: ArrayList<String> = arrayListOf()

        if (screen.isChecked){
            myTagList.add(screen.text.toString())
        }

        if (baterry.isChecked){
            myTagList.add(baterry.text.toString())
        }

        if (diagnostic.isChecked){
            myTagList.add(diagnostic.text.toString())
        }

        if (damage.isChecked){
            myTagList.add(damage.text.toString())
        }

        if (waterDamage.isChecked){
            myTagList.add(waterDamage.text.toString())
        }

        if (dataRepeir.isChecked){
            myTagList.add(dataRepeir.text.toString())
        }

        if (corpus.isChecked){
            myTagList.add(corpus.text.toString())
        }

        if (temperature.isChecked){
            myTagList.add(temperature.text.toString())
        }
        if (po.isChecked){
            myTagList.add(po.text.toString())
        }

        var services = myTagList.map { it }.joinToString()
        if (myTagList.isEmpty()){
            services = "Другое"
        }
        changes?.put("id", Prefs.getInt("service_id", 0))
        changes?.put("title", title.text.toString())
        changes?.put("address", address.text.toString())
        changes?.put("services", services)
        changes?.put("description", aboutService.text.toString())

        infoDisposable = apiPulicService.value.updateServiceInfo(changes)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: Any ->

                    goBack()
                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }
}