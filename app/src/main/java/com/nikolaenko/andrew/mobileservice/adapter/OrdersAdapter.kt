package com.nikolaenko.andrew.mobileservice.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.model.OrderModel
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import com.nikolaenko.andrew.mobileservice.model.PriceModel
import com.nikolaenko.andrew.mobileservice.model.ServiceResponse
import com.nikolaenko.andrew.mobileservice.network.APIPublicService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.zipWith
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*

class OrdersAdapter (private val callbackReview: ((id: Int) -> Unit)? = null) : RecyclerView.Adapter<OrdersAdapter.MyViewHolder>() {

    private var list: MutableList<OrderModelNew> = ArrayList()

    fun setList(list: MutableList<OrderModelNew>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var title: TextView = view.find(R.id.title)
        var date: TextView = view.find(R.id.date)
        var note: TextView = view.find(R.id.note)
        var price: TextView = view.find(R.id.price)
        var logo: ImageView = view.find(R.id.logo)

        var review: TextView = view.find(R.id.review)
        var item = view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_for_order, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]

        val kodein = LazyKodein(holder.item.context.appKodein)
        val apiPulicService = kodein.instance<APIPublicService>()

        apiPulicService.value.getServiceInfo(item.serviceId!!)
                .zipWith(apiPulicService.value.getPriceList()) { t1, t2 ->
                    Pair(t1, t2)
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: Pair<ServiceResponse, List<PriceModel>> ->

                    holder.title.text = t.first.title

                    if (t.first.logo == "images/no_photo.jpg" || t.first.logo == "imgBase64"){
                        Glide.with(holder.itemView.context).load(R.drawable.meme_determined).into(holder.logo)
                    } else {

                        val url = holder.itemView.context.getString(R.string.api_base_url) + t.first.logo + ".png"
                        Glide.with(holder.itemView.context).load(url).into(holder.logo)
                    }

                    val newList = arrayListOf<PriceModel>()
                    newList.clear()

                    for (driverTest in t.second){
                        if (driverTest.serviceId == item.serviceId!!){

                            val price = driverTest.price.toString()
                            if (price != null){
                                holder.price.text = "$price руб."
                            } else {
                                holder.price.text = "0 руб."
                            }

                        }
                    }

                    val date = Date(item.createdAt!!)
                    val df2 = SimpleDateFormat("dd/MM/yy")
                    val dateText = df2.format(date)

                    holder.date.text = dateText
                    holder.note.text = item.description

                }, { e ->
                    e.printStackTrace()
                })

        if (item.status == "complete" && item.feedback.isNullOrEmpty()){
            holder.review.visibility = View.VISIBLE
        } else {
            holder.review.visibility = View.GONE
        }

        holder.review.setOnClickListener {
            callbackReview?.invoke(item.id!!)
        }
    }

    override fun getItemCount(): Int = list.size
}
