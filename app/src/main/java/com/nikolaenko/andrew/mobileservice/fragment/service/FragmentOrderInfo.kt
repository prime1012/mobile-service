package com.nikolaenko.andrew.mobileservice.fragment.service

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.order_info_fragment.*
import org.jetbrains.anko.support.v4.toast

class FragmentOrderInfo : BaseFragment() {

    private var infoDisposable: Disposable? = null

    private var id: Int? = null
    private var repairType: String? = null
    private var description: String? = null
    private var deviceType: String? = null
    private var brand: String? = null
    private var address: String? = null
    private var photo: String? = null
    private var time: String? = null

    override fun onStart() {
        super.onStart()

        id = arguments?.getInt(ARG_ID)
        repairType = arguments?.getString(ARG_REP_TYPE)
        description = arguments?.getString(ARG_DESCRIPTION)
        deviceType = arguments?.getString(ARG_DEV_TYPE)
        brand = arguments?.getString(ARG_BRAND)
        address = arguments?.getString(ARG_ADDRESS)
        photo = arguments?.getString(ARG_PHOTO)
        time = arguments?.getString(ARG_TIME)

        toolbarTitle.text = id.toString()

//        if (photo == "images/no_photo.jpg" || photo == "imgBase64"){
//            Glide.with(this).load(R.drawable.meme_determined).into(image)
//        } else {
//
//            val decodedString = Base64.decode(photo, Base64.URL_SAFE)
//            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
//            Glide.with(this).asBitmap().load(decodedByte).into(image)
//        }

        if (photo == "images/no_photo.jpg" || photo == "imgBase64"){
            Glide.with(this).load(R.drawable.meme_determined).into(image)
        } else {

            val url = getString(R.string.api_base_url) + photo + ".png"
            Glide.with(this).load(url).into(image)
        }

        repairTypeTV.text = repairType
        descriptionTV.text = description
        deviceTypeTV.text = deviceType
        brandTV.text = brand
        addressTV.text = address
        timeTV.text = time
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.order_info_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        acceptBtn.setOnClickListener {

            showProgress(true)

            infoDisposable = apiPulicService.value.acceptOrder(id!!)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: List<OrderModelNew> ->

                        mainActivity.hardReplaceFragment(FragmentSeviceScreen())
                        showProgress(false)
                    }, { e ->
                        toast(e.message.toString())
                        e.printStackTrace()
                        showProgress(false)
                    })
        }

        rejectBtn.setOnClickListener {
            showProgress(true)
            infoDisposable = apiPulicService.value.rejectOrder(id!!)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: List<OrderModelNew> ->

                        mainActivity.hardReplaceFragment(FragmentSeviceScreen())
                        showProgress(false)
                    }, { e ->
                        toast(e.message.toString())
                        e.printStackTrace()
                        showProgress(false)
                    })
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }

    }

    companion object {

        private val ARG_ID = "arg_user_id"
        private val ARG_REP_TYPE = "arg_repType"
        private val ARG_DESCRIPTION = "arg_desc"
        private val ARG_DEV_TYPE = "arg_devType"
        private val ARG_BRAND = "arg_brand"
        private val ARG_ADDRESS = "arg_address"
        private val ARG_PHOTO = "arg_photo"
        private val ARG_TIME = "arg_time"

        fun newInstance(id: Int, repairType: String, description: String, deviceType: String, brand: String, address: String, photo: String, time: String): FragmentOrderInfo {

            val args = Bundle()
            args.putInt(ARG_ID, id)
            args.putString(ARG_REP_TYPE, repairType)
            args.putString(ARG_DESCRIPTION, description)
            args.putString(ARG_DEV_TYPE, deviceType)
            args.putString(ARG_BRAND, brand)
            args.putString(ARG_ADDRESS, address)
            args.putString(ARG_PHOTO, photo)
            args.putString(ARG_TIME, time)

            val fragment = FragmentOrderInfo()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }
}