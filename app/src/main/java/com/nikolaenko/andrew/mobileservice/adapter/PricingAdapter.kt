package com.nikolaenko.andrew.mobileservice.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.model.PriceModel
import org.jetbrains.anko.find


class PricingAdapter : RecyclerView.Adapter<PricingAdapter.MyViewHolder>() {

    private var list: MutableList<PriceModel> = ArrayList()

    fun setList(list: MutableList<PriceModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.find(R.id.title)
        var amountText: TextView = view.find(R.id.amountText)
        var item = view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_for_price, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]

        holder.title.text = item.title
        holder.amountText.text = item.price.toString() + " руб."
    }

    override fun getItemCount(): Int = list.size
}
