package com.nikolaenko.andrew.mobileservice.fragment.service

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.adapter.PricingAdapter
import com.nikolaenko.andrew.mobileservice.adapter.ServiceOrdersAdapter
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import com.nikolaenko.andrew.mobileservice.model.PriceModel
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.all_orders_fragment.*
import org.jetbrains.anko.support.v4.toast

class FragmentAllOrders : BaseFragment() {

    private var infoDisposable: Disposable? = null

    private lateinit var adapter: ServiceOrdersAdapter
    private var status: String? = null

    override fun onStart() {
        super.onStart()
        status = arguments?.getString(ARG_STATUS)

        getServiceInfo()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.all_orders_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ServiceOrdersAdapter({
            id, repairType, description, deviceType, brand, address,  photo, time ->
            replaceFragment(FragmentOrderInfo.newInstance(id, repairType, description, deviceType, brand, address, photo, time), true)
        }, {
            id, repairType, description, deviceType, brand, address, photo, time ->
            replaceFragment(FragmentUpdateOrderInfo.newInstance(id, repairType, description, deviceType, brand, address, photo, time), true)
        })

        rvPrices.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
        rvPrices.itemAnimator = DefaultItemAnimator()
        rvPrices.adapter = adapter

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }
    }

    private fun getServiceInfo(){

        infoDisposable = apiPulicService.value.getServiceOrders(Prefs.getInt("service_id", 0))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<OrderModelNew> ->

                    if (status == "sent"){

                        val newList = arrayListOf<OrderModelNew>()
                        newList.clear()

                        for (driverTest in t){
                            if (driverTest.status == "sent"){
                                newList.add(driverTest)
                            }
                        }

                        adapter.setList(newList)
                    } else if ( status == "accepted"){

                        val newList = arrayListOf<OrderModelNew>()
                        newList.clear()

                        for (driverTest in t){
                            if (driverTest.status == "accepted"){
                                newList.add(driverTest)
                            }
                        }

                        adapter.setList(newList)
                    }

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })

    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }

    companion object {

        private val ARG_STATUS = "arg_status"


        fun newInstance(status: String): FragmentAllOrders {

            val args = Bundle()
            args.putString(ARG_STATUS, status)

            val fragment = FragmentAllOrders()
            fragment.arguments = args
            return fragment
        }
    }
}