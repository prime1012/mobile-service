package com.nikolaenko.andrew.mobileservice.model

import com.google.gson.annotations.SerializedName

class ModelsResponse<T> {

    @SerializedName("data")
    var data: List<T>? = null

    @SerializedName("code")
    var code: Int? = null
}
