package com.nikolaenko.andrew.mobileservice.model

import android.graphics.drawable.Drawable
import com.google.gson.annotations.SerializedName

class ServicesModel{
    @SerializedName("createdAt")
    var createdAt: Long? = null

    @SerializedName("updatedAt")
    var updatedAt: Long? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("surname")
    var surname: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("phoneAdditional")
    var phoneAdditional: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("password")
    var password: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("services")
    var services: String? = null

    @SerializedName("logo")
    var logo: String? = null

    @SerializedName("description")
    var description: String? = null
}
