package com.nikolaenko.andrew.mobileservice.fragment.service


import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.ServiceResponse
import com.pixplicity.easyprefs.library.Prefs
import com.redmadrobot.inputmask.MaskedTextChangedListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.service_sign_up_fragment.*
import org.jetbrains.anko.support.v4.toast
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.HashMap


class ServiceSignUpFragment : BaseFragment() {

    private var regDisposable: Disposable? = null
    internal var phoneNumber: String? = null
    internal var phoneNumberFilled: Boolean = false

    private var workingTime: String? = null
    var myTagList: ArrayList<String> = arrayListOf()
    private var isGranted: Boolean = false
    private var changes: HashMap<String, Any?>? = null
    private var imageBase64: String? = null
    private var serv: String? = null
    private var selectedImageURI: Uri? = null

    override fun onStart() {
        super.onStart()
        showPermissions()
        changes = HashMap()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.service_sign_up_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftButton.setOnClickListener {
            goBack()
        }

        editFirstName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutSurname.visibility = View.VISIBLE
                } else {
                    layoutSurname.visibility = View.GONE
                }
            }

        })

        editLastName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutPhone.visibility = View.VISIBLE
                } else {
                    layoutPhone.visibility = View.GONE
                }
            }

        })

        editPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutSecondPhone.visibility = View.VISIBLE
                    layoutEmail.visibility = View.VISIBLE
                } else {
                    layoutSecondPhone.visibility = View.GONE
                    layoutEmail.visibility = View.GONE
                }
            }

        })

        editEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutPassword.visibility = View.VISIBLE
                    layoutSecondPassword.visibility = View.VISIBLE
                } else {
                    layoutPassword.visibility = View.GONE
                    layoutSecondPassword.visibility = View.GONE
                }
            }

        })

        editPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {

            }

        })

        editSecondPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutCompanyName.visibility = View.VISIBLE
                } else {
                    layoutCompanyName.visibility = View.GONE
                }
            }

        })

        editCompanyName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutAddress.visibility = View.VISIBLE
                    layoutServices.visibility = View.VISIBLE
                    layoutLogo.visibility = View.VISIBLE
                    layoutAboutCompany.visibility = View.VISIBLE
                } else {
                    layoutAddress.visibility = View.GONE
                    layoutServices.visibility = View.GONE
                    layoutLogo.visibility = View.GONE
                    layoutAboutCompany.visibility = View.GONE
                }
            }

        })

        screen.setOnClickListener {
            myTagList.add(screen.text.toString())
            workLay.visibility = View.GONE
        }

        baterry.setOnClickListener {
            myTagList.add(baterry.text.toString())
            workLay.visibility = View.GONE
        }
        diagnostic.setOnClickListener {
            myTagList.add(diagnostic.text.toString())
            workLay.visibility = View.GONE
        }
        damage.setOnClickListener {
            myTagList.add(damage.text.toString())
            workLay.visibility = View.GONE
        }
        waterDamage.setOnClickListener {
            myTagList.add(waterDamage.text.toString())
            workLay.visibility = View.GONE
        }
        dataRepeir.setOnClickListener {
            myTagList.add(dataRepeir.text.toString())
            workLay.visibility = View.GONE
        }
        corpus.setOnClickListener {
            myTagList.add(corpus.text.toString())
            workLay.visibility = View.GONE
        }
        temperature.setOnClickListener {
            myTagList.add(temperature.text.toString())
            workLay.visibility = View.GONE
        }
        po.setOnClickListener {
            workLay.visibility = View.GONE
            myTagList.add(po.text.toString())
        }
        other.setOnClickListener {
            workLay.visibility = View.VISIBLE
        }

        editWork.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                serv = p0.toString()
            }

        })

        editAboutCompany.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()){
                    layoutTime.visibility = View.VISIBLE
                } else {
                    layoutTime.visibility = View.GONE
                }
            }

        })

        time1.setOnClickListener {
            continueBtn.visibility = View.VISIBLE
            workingTime = time1.text.toString()
            time2.isChecked = false
            time3.isChecked = false
        }
        time2.setOnClickListener {
            continueBtn.visibility = View.VISIBLE
            workingTime = time2.text.toString()
            time1.isChecked = false
            time3.isChecked = false
        }
        time3.setOnClickListener {
            continueBtn.visibility = View.VISIBLE
            workingTime = time3.text.toString()
            time1.isChecked = false
            time2.isChecked = false
        }

        val listener = MaskedTextChangedListener(
                "373 [000]-[00]-[00]",
                false,
                editPhone,
                null,
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(maskFilled: Boolean, extractedValue: String) {
                        phoneNumber = extractedValue
                        phoneNumberFilled = maskFilled
                    }
                }
        )

        editPhone.addTextChangedListener(listener)
        editPhone.onFocusChangeListener = listener
        editPhone.hint = listener.placeholder()

        layoutLogo.setOnClickListener {
            if (isGranted){

                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE)

            } else {
                showPermissions()
            }
        }

        continueBtn.setOnClickListener {

            if (editPassword.text.toString() != editSecondPassword.text.toString()){
                toast("Пароли не совпадают")
                return@setOnClickListener
            }

            showProgress(true)

            if (other.isChecked && serv != null) {
                myTagList.add(serv!!)
            }

            val services = myTagList.map { it }.joinToString()

            changes?.put("name", editFirstName.text.toString())
            changes?.put("surname", editLastName.text.toString())

            val phNumb = editPhone.text.toString()
            val phoneNumb = phNumb.replace(" ", "")
            val phoneNumb2 = phoneNumb.replace("-", "")

            changes?.put("phone", phoneNumb2)
            changes?.put("phoneAdditional", editSecondPhone.text.toString())
            changes?.put("email", editEmail.text.toString())
            changes?.put("password", editPassword.text.toString())
            changes?.put("title", editCompanyName.text.toString())
            changes?.put("address", addressValue.text.toString())
            changes?.put("services", services)

            changes?.put("logo", "data:image/png;base64," + encode(selectedImageURI!!))
            changes?.put("description", editAboutCompany.text.toString())

            regDisposable = apiPulicService.value.serviceSignUp(changes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: List<ServiceResponse> ->

                        if (t != null){
                            if (t[0].email != null && t[0].password != null){
                                apiPulicService.value.serviceSignIn(t[0].email!!, t[0].password!!)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({ t: ServiceResponse ->

                                            Prefs.putInt("service_id", t.id!!)

                                            mainActivity.hardReplaceFragment(FragmentSeviceScreen())
                                            showProgress(false)

                                        }, { e ->
                                            toast(e.message.toString())
                                            e.printStackTrace()
                                            showProgress(false)
                                        })
                            }
                        }

                    }, { e ->
                        toast(e.message.toString())
                        e.printStackTrace()
                        showProgress(false)
                    })
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_CODE) {

                selectedImageURI = data?.data

                if (selectedImageURI != null){
                    logo.visibility = View.VISIBLE
                    Glide.with(this).load(selectedImageURI).into(logo)

                    val imageStream = context?.contentResolver?.openInputStream(selectedImageURI)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
//                    imageBase64 = encodeImage(selectedImage)

//                    imageBase64 = encode(selectedImageURI)
                } else {
                    logo.visibility = View.GONE
                }

            }

        }
    }

    private fun showPermissions(){

        Dexter.withActivity(mainActivity)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        try {

                            isGranted = report!!.areAllPermissionsGranted()

                        } catch (e: IOException) {
                            toast("Could not create file!")
                        }
                    }
                }).check()
    }

    private fun encodeImage(bm: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()

        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    override fun onStop() {
        super.onStop()
        regDisposable?.dispose()
    }

    fun encode(imageUri: Uri): String {
        val input = activity?.contentResolver?.openInputStream(imageUri)
        val image = BitmapFactory.decodeStream(input , null, null)
        //encode image to base64 string
        val baos = ByteArrayOutputStream()
        image?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageBytes = baos.toByteArray()
        val imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
        return imageString
    }

    companion object {
        private val GALLERY_REQUEST_CODE = 100
    }
}
