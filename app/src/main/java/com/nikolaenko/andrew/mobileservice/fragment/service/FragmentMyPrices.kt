package com.nikolaenko.andrew.mobileservice.fragment.service

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.adapter.PricingAdapter
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.PriceModel
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.my_prising_fragment.*
import org.jetbrains.anko.support.v4.toast

class FragmentMyPrices : BaseFragment() {

    private var infoDisposable: Disposable? = null
    private var id: Int? = null
    private lateinit var adapter: PricingAdapter
    override fun onStart() {
        super.onStart()

        id = Prefs.getInt("service_id", 0)

        getServiceInfo()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.my_prising_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = PricingAdapter()
        rvPrices.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
        rvPrices.itemAnimator = DefaultItemAnimator()
        rvPrices.adapter = adapter

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }

    }

    private fun getServiceInfo(){

        infoDisposable = apiPulicService.value.getPriceList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<PriceModel> ->

                    val newList = arrayListOf<PriceModel>()
                    newList.clear()

                    for (driverTest in t){
                        if (driverTest.serviceId == id){
                            newList.add(driverTest)
                        }
                    }

                    adapter.setList(newList)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })

    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }
}