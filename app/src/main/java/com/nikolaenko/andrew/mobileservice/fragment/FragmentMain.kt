package com.nikolaenko.andrew.mobileservice.fragment


import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.nikolaenko.andrew.mobileservice.R
import kotlinx.android.synthetic.main.start_fragment.*
import org.jetbrains.anko.support.v4.toast

class FragmentMain : BaseFragment() {

    private var userType: Int = 0
    private var isGranted: Boolean = false

    override fun onStart() {
        super.onStart()
        getCurrentLocation()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.start_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clientBtn.setOnClickListener {
            if (isGranted){
                userType = 0
                replaceFragment(FragmentAutorize.newInstance(userType), true)
            } else {
                toast("Разрешите сначала доступ к вашей геолокации!!!")
                getCurrentLocation()
                return@setOnClickListener
            }
        }

        serviceBtn.setOnClickListener {
            if (isGranted){
                userType = 1
                replaceFragment(FragmentAutorize.newInstance(userType), true)
            }else {
                toast("Разрешите сначала доступ к вашей геолокации!!!")
                getCurrentLocation()
                return@setOnClickListener
            }
        }

        driverBtn.setOnClickListener {
            if (isGranted){
                userType = 2
                replaceFragment(FragmentAutorize.newInstance(userType), true)
            }else {
                toast("Разрешите сначала доступ к вашей геолокации!!!")
                getCurrentLocation()
                return@setOnClickListener
            }
        }
    }

    private fun getCurrentLocation(){
        Dexter.withActivity(mainActivity)
                .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        isGranted = report?.areAllPermissionsGranted()!!
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        token!!.continuePermissionRequest()
                    }

                }).check()
    }

}
