package com.nikolaenko.andrew.mobileservice.fragment

import android.app.Dialog
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.adapter.OrdersAdapter
import com.nikolaenko.andrew.mobileservice.fragment.client.FragmentHomeScreen
import com.nikolaenko.andrew.mobileservice.model.OrderModel
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_orders.*
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.toast
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.List
import kotlin.collections.arrayListOf
import kotlin.collections.toMutableList

class FragmentOrders : BaseFragment() {

    private lateinit var adapter: OrdersAdapter
    private var fromWhere: String? = null
    private var ordersInfoDisposable: Disposable? = null

    var listOrder = arrayListOf<OrderModelNew>()
    private var changes: HashMap<String, Any?>? = null
    override fun onStart() {
        super.onStart()

        fromWhere = arguments?.getString(ARG_FROM_WHERE)

        changes = HashMap()

        if (fromWhere == "my_orders"){
            toolbarTitle.text = getString(R.string.my_orders)
            imagetLayout.visibility = View.VISIBLE
            getMyOrders()
        } else if (fromWhere == "service_all"){
            toolbarTitle.text = getString(R.string.requests_)
            imagetLayout.visibility = View.GONE
            getAllOrders()
        } else {
            toolbarTitle.text = getString(R.string.requests_)
            imagetLayout.visibility = View.GONE
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_orders, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = OrdersAdapter{
            id ->
            showDialogAction(id)
        }

        rvOrders.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvOrders.itemAnimator = DefaultItemAnimator()
        rvOrders.adapter = adapter
        rvOrders.isNestedScrollingEnabled = false


        sendedImage.setOnClickListener {
            val newList = arrayListOf<OrderModelNew>()
            newList.clear()

            for (driverTest in listOrder){
                if (driverTest.status == "sent"){
                    newList.add(driverTest)
                }
            }

            adapter.setList(newList)
        }
        waitingImage.setOnClickListener {
            val newList = arrayListOf<OrderModelNew>()
            newList.clear()

            for (driverTest in listOrder){
                if (driverTest.status == "accepted"){
                    newList.add(driverTest)
                }
            }

            adapter.setList(newList)
        }
        finishedImage.setOnClickListener {
            val newList = arrayListOf<OrderModelNew>()
            newList.clear()

            for (driverTest in listOrder){
                if (driverTest.status == "complete"){
                    newList.add(driverTest)
                }
            }

            adapter.setList(newList)
        }
        declinedImage.setOnClickListener {
            val newList = arrayListOf<OrderModelNew>()
            newList.clear()

            for (driverTest in listOrder){
                if (driverTest.status == "rejected"){
                    newList.add(driverTest)
                }
            }

            adapter.setList(newList)
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }

    }

    private fun getMyOrders(){
        showProgress(true)
        ordersInfoDisposable = apiPulicService.value.getMyOrders(Prefs.getInt("customer_id", 0))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<OrderModelNew> ->

                    listOrder = t as ArrayList<OrderModelNew>

                    val newList = arrayListOf<OrderModelNew>()
                    newList.clear()

                    for (driverTest in listOrder){
                        if (driverTest.status == "sent"){
                            newList.add(driverTest)
                        }
                    }

                    adapter.setList(newList)

                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    private fun getAllOrders(){

        showProgress(true)
        ordersInfoDisposable = apiPulicService.value.getServiceOrders(Prefs.getInt("service_id", 0))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<OrderModelNew> ->

                    adapter.setList(t.toMutableList())

                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })

    }

    private fun sendReviewToService(id: Int, text: String){

        showProgress(true)

        changes?.put("id", id)
        changes?.put("feedback", text)

        ordersInfoDisposable = apiPulicService.value.updateOrder(changes)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<OrderModelNew> ->

                    mainActivity.hardReplaceFragment(FragmentHomeScreen())
                    toast("Вы отправили отзыв!")
                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })

    }

    private fun showDialogAction(id: Int){

        val mDialog = Dialog(context, R.style.CustomDialog)
        mDialog.setCancelable(true)
        mDialog.setContentView(R.layout.dialog_with_action)

        val firstButtonText = mDialog.find<TextView>(R.id.firstButtonText)
        val secondButtonText = mDialog.find<TextView>(R.id.secondButtonText)
        val note = mDialog.find<EditText>(R.id.note)

        firstButtonText.setOnClickListener {
            mDialog.dismiss()
        }

        secondButtonText.setOnClickListener {
            mDialog.dismiss()
            sendReviewToService(id, note.text.toString().trim())
        }

        mDialog.show()
    }

    companion object {

        private val ARG_FROM_WHERE = "arg_from_where"

        val lstDone = ArrayList<OrderModel>()
        val lstInProgress = ArrayList<OrderModel>()
        val lstWaiting = ArrayList<OrderModel>()
        val lstDeclined = ArrayList<OrderModel>()

        fun newInstance(fromWhere: String): FragmentOrders {

            val args = Bundle()
            args.putString(ARG_FROM_WHERE, fromWhere)

            val fragment = FragmentOrders()
            fragment.arguments = args
            return fragment
        }
    }
}