package com.nikolaenko.andrew.mobileservice.fragment.service

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.*
import com.bumptech.glide.Glide
import com.nikolaenko.andrew.mobileservice.fragment.FragmentOrders
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.ServiceResponse
import com.nikolaenko.andrew.mobileservice.model.ServicesModel
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home_for_service.*
import org.jetbrains.anko.support.v4.toast

class FragmentSeviceScreen : BaseFragment() {

    private var infoDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()

        val serviceId = Prefs.getInt("service_id", 0)

        getServiceInfo(serviceId)

        workingHours.text = Prefs.getString("service_time", null)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home_for_service, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.service_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.info -> {
                replaceFragment(FragmentSeviceEdit(), true)
                return true
            }
            R.id.prices -> {
                replaceFragment(FragmentPrices(), true)
                return true
            }
            R.id.pricesEdit -> {
                replaceFragment(FragmentMyPrices(), true)
                return true
            }

            R.id.tasks -> {
                replaceFragment(FragmentAllOrders.newInstance("sent"), true)
                return true
            }
            R.id.inProgress -> {
                replaceFragment(FragmentAllOrders.newInstance("accepted"), true)
                return true
            }
        }
        return false
    }

    private fun getServiceInfo(serviceId: Int){

        infoDisposable = apiPulicService.value.getServiceInfo(serviceId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: ServiceResponse ->

                    if (t != null){
                        t.title?.let {  toolbarTitle.text = it }

                        if (t.logo == "images/no_photo.jpg" || t.logo == "imgBase64"){
                            Glide.with(this).load(R.drawable.meme_determined).into(logo)
                        } else {

                            val url = getString(R.string.api_base_url) + t.logo + ".png"
                            Glide.with(this).load(url).into(logo)
                        }

                        t.title?.let { title.text = it }
                        t.address?.let { address.text = it }
                        t.services?.let { repairType.text = it }
                        t.description?.let { aboutService.text = it }

                    }


                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })

    }

    companion object {

        private val ARG_SERVICE_ID = "arg_service_id"


        fun newInstance(serviceId: Int): FragmentSeviceScreen {

            val args = Bundle()
            args.putInt(ARG_SERVICE_ID, serviceId)

            val fragment = FragmentSeviceScreen()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }

//    fun decode(imageString: String) {
//
//        //decode base64 string to image
//        val imageBytes = Base64.decode(imageString, Base64.DEFAULT)
//        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
//
//        logo.setImageBitmap(decodedImage)
//    }
}