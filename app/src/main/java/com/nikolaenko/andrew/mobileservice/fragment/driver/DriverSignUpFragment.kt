package com.nikolaenko.andrew.mobileservice.fragment.driver


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.DriverResponse
import com.pixplicity.easyprefs.library.Prefs
import com.redmadrobot.inputmask.MaskedTextChangedListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.driver_sign_up_fragment.*
import org.jetbrains.anko.support.v4.toast
import java.util.HashMap


class DriverSignUpFragment : BaseFragment() {

    private var regDisposable: Disposable? = null
    internal var phoneNumber: String? = null
    internal var phoneNumberFilled: Boolean = false

    private var whereWork: String? = null
    private var isAdult: Boolean? = false
    private var isAlowedToDrive: Boolean? = false
    private var hasViolation: Boolean? = false
    private var license: Boolean? = false
    private var vehicle: String? = null
    private var previousWork: String? = null
    private var workingTime: String? = null

    private var changes: HashMap<String, Any?>? = null

    override fun onStart() {
        super.onStart()

        changes = HashMap()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.driver_sign_up_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftButton.setOnClickListener {
            goBack()
        }

        editFirstName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()) {
                    layoutSurname.visibility = View.VISIBLE
                } else {
                    layoutSurname.visibility = View.GONE
                }
            }

        })

        editLastName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()) {
                    layoutPhone.visibility = View.VISIBLE
                } else {
                    layoutPhone.visibility = View.GONE
                }
            }

        })

        editPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()) {
                    layoutSecondPhone.visibility = View.VISIBLE
                    layoutEmail.visibility = View.VISIBLE
                } else {
                    layoutSecondPhone.visibility = View.GONE
                    layoutEmail.visibility = View.GONE
                }
            }

        })

        editEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()) {
                    layoutPassword.visibility = View.VISIBLE
                    layoutSecondPassword.visibility = View.VISIBLE
                } else {
                    layoutPassword.visibility = View.GONE
                    layoutSecondPassword.visibility = View.GONE
                }
            }

        })

        editPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {

            }

        })

        editSecondPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()) {
                    layoutWork.visibility = View.VISIBLE
                } else {
                    layoutWork.visibility = View.GONE
                }
            }

        })

        val listener = MaskedTextChangedListener(
                "373 [000]-[00]-[00]",
                false,
                editPhone,
                null,
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(maskFilled: Boolean, extractedValue: String) {
                        phoneNumber = extractedValue
                        phoneNumberFilled = maskFilled
                    }
                }
        )

        editPhone.addTextChangedListener(listener)
        editPhone.onFocusChangeListener = listener
        editPhone.hint = listener.placeholder()

        workButton.setOnClickListener {
            if (workButton.isChecked) {

                whereWork = workButton.text.toString()
                layoutAdult.visibility = View.VISIBLE

            } else {

                layoutAdult.visibility = View.GONE
            }
        }

        aduldYes.setOnClickListener {
            isAdult = true
            aduldNo.isChecked = false
            layoutLicense.visibility = View.VISIBLE
        }

        aduldNo.setOnClickListener {
            isAdult = false
            aduldYes.isChecked = false
            layoutLicense.visibility = View.VISIBLE
        }

        licenseYes.setOnClickListener {
            //            layoutCar.visibility = View.VISIBLE
            licenseNo.isChecked = false
            license = true

            layoutIsAllowToDrive.visibility = View.VISIBLE
        }

        licenseNo.setOnClickListener {
            //            layoutCar.visibility = View.VISIBLE
            licenseYes.isChecked = false
            license = false

            layoutIsAllowToDrive.visibility = View.VISIBLE
        }

        editLicense.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (p0.isNotEmpty()) {
                    layoutCar.visibility = View.VISIBLE
                } else {
                    layoutCar.visibility = View.GONE
                }
            }

        })

        carYes.setOnClickListener {
            layoutIncident.visibility = View.VISIBLE
            vehicle = carYes.text.toString()
            carNo.isChecked = false
            carRent.isChecked = false
        }
        carNo.setOnClickListener {
            layoutIncident.visibility = View.VISIBLE
            vehicle = carYes.text.toString()
            carYes.isChecked = false
            carRent.isChecked = false
        }
        carRent.setOnClickListener {
            layoutIncident.visibility = View.VISIBLE
            vehicle = carYes.text.toString()
            carYes.isChecked = false
            carNo.isChecked = false
        }

        incidentYes.setOnClickListener {
            layoutWorkExperience.visibility = View.VISIBLE
            hasViolation = true
            incidentNo.isChecked = false
        }
        incidentNo.setOnClickListener {
            layoutWorkExperience.visibility = View.VISIBLE
            hasViolation = false
            incidentYes.isChecked = false
        }

        exp1.setOnClickListener {
            layoutTime.visibility = View.VISIBLE
            previousWork = exp1.text.toString()
            workLay.visibility = View.GONE
            exp2.isChecked = false
            exp3.isChecked = false
            exp4.isChecked = false
            exp5.isChecked = false
        }
        exp2.setOnClickListener {
            layoutTime.visibility = View.VISIBLE
            previousWork = exp2.text.toString()
            workLay.visibility = View.GONE
            exp1.isChecked = false
            exp3.isChecked = false
            exp4.isChecked = false
            exp5.isChecked = false
        }
        exp3.setOnClickListener {
            layoutTime.visibility = View.VISIBLE
            previousWork = exp3.text.toString()
            workLay.visibility = View.GONE
            exp1.isChecked = false
            exp2.isChecked = false
            exp4.isChecked = false
            exp5.isChecked = false
        }
        exp4.setOnClickListener {
            layoutTime.visibility = View.VISIBLE
            previousWork = exp4.text.toString()
            workLay.visibility = View.GONE
            exp1.isChecked = false
            exp2.isChecked = false
            exp3.isChecked = false
            exp5.isChecked = false
        }
        exp5.setOnClickListener {

            workLay.visibility = View.VISIBLE
            exp1.isChecked = false
            exp2.isChecked = false
            exp3.isChecked = false
            exp4.isChecked = false
        }

        editWork.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (p0!!.length > 3){
                    layoutTime.visibility = View.VISIBLE
                } else {
                    layoutTime.visibility = View.GONE
                }

                previousWork = p0.toString()
            }
        })



        time1.setOnClickListener {
            continueBtn.visibility = View.VISIBLE
            workingTime = time1.text.toString()
            time2.isChecked = false
            time3.isChecked = false
        }
        time2.setOnClickListener {
            continueBtn.visibility = View.VISIBLE
            workingTime = time2.text.toString()
            time1.isChecked = false
            time3.isChecked = false
        }
        time3.setOnClickListener {
            continueBtn.visibility = View.VISIBLE
            workingTime = time3.text.toString()
            time1.isChecked = false
            time2.isChecked = false
        }

        continueBtn.setOnClickListener {

            if (editPassword.text.toString() != editSecondPassword.text.toString()) {
                toast("Пароли не совпадают")
                return@setOnClickListener
            }

            showProgress(true)

            changes?.put("name", editFirstName.text.toString())
            changes?.put("surname", editLastName.text.toString())

            val phNumb = editPhone.text.toString()
            val phoneNumb = phNumb.replace(" ", "")
            val phoneNumb2 = phoneNumb.replace("-", "")

            changes?.put("phone", phoneNumb2)
            changes?.put("phoneAdditional", editSecondPhone.text.toString())
            changes?.put("email", editEmail.text.toString())
            changes?.put("password", editPassword.text.toString())

            changes?.put("wishesToWork", whereWork)
            changes?.put("isAdult", isAdult)
            changes?.put("isAlowedToDrive", license)
            changes?.put("licenseId", editLicense.text.toString())
            changes?.put("vehicle", vehicle)
            changes?.put("hasViolation", hasViolation)
            changes?.put("previousWork", previousWork)
            changes?.put("workingTime", workingTime)

            regDisposable = apiPulicService.value.driverSignUp(changes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: DriverResponse ->

                        if (t != null){
                            if (t.email != null && t.password != null) {


                                apiPulicService.value.driverSignIn(t.email!!, t.password!!)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({ t: DriverResponse ->

                                            Prefs.putInt("driver_id", t.id!!)
                                            mainActivity.hardReplaceFragment(FragmentDriverMainScreen())

                                            showProgress(false)

                                        }, { e ->
                                            toast(e.message.toString())
                                            e.printStackTrace()
                                            showProgress(false)

                                        })
                            }
                        }

                    }, { e ->
                        toast(e.message.toString())
                        e.printStackTrace()
                        showProgress(false)
                    })
        }
    }

    override fun onStop() {
        super.onStop()
        regDisposable?.dispose()
    }
}
