package com.nikolaenko.andrew.mobileservice.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.client.ClientSignUpFragment
import com.nikolaenko.andrew.mobileservice.fragment.driver.DriverSignUpFragment
import com.nikolaenko.andrew.mobileservice.fragment.service.ServiceSignUpFragment
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.authenticate_fragment.*

class FragmentAutorize : BaseFragment() {

    private var userType: Int = 0

    override fun onStart() {
        super.onStart()
        Prefs.clear()
        userType = arguments?.getInt(ARG_USER_TYPE)!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.authenticate_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signInBtn.setOnClickListener {
            replaceFragment(FragmentSignIn.newInstance(userType), true)
        }

        signUpBtn.setOnClickListener {
            when(userType){
                1-> {replaceFragment(ServiceSignUpFragment(), true)}
                2-> {replaceFragment(DriverSignUpFragment(), true)}
                else -> {replaceFragment(ClientSignUpFragment(), true)}
            }
        }

    }
    companion object {

        private val ARG_USER_TYPE = "arg_client"

        fun newInstance(userType: Int): FragmentAutorize {

            val args = Bundle()
            args.putInt(ARG_USER_TYPE, userType)

            val fragment = FragmentAutorize()
            fragment.arguments = args
            return fragment
        }
    }

}
