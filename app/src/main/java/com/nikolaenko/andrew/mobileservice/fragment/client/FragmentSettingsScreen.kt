package com.nikolaenko.andrew.mobileservice.fragment.client

import android.Manifest
import android.os.Bundle
import android.view.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_settings_screen.*
import org.jetbrains.anko.support.v4.toast
import java.io.IOException

class FragmentSettingsScreen : BaseFragment() {

    private var isGranted: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings_screen, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvMyProfile.setOnClickListener {
            replaceFragment(FragmentMyProfile(), true)
        }

        tvLocation.setOnClickListener {

            if (isGranted) {
                toast("Вы разрешили использование геолокации!")
            } else {
                showPermissions()
            }

        }

    }

    private fun showPermissions(){

        Dexter.withActivity(mainActivity)
                .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        try {

                            isGranted = report!!.areAllPermissionsGranted()

                        } catch (e: IOException) {
                            toast("Could not create file!")
                        }
                    }
                }).check()
    }
}