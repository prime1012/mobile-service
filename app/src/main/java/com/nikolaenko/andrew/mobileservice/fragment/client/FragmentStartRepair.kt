package com.nikolaenko.andrew.mobileservice.fragment.client

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.AdapterView
import com.nikolaenko.andrew.mobileservice.R
import kotlinx.android.synthetic.main.fragment_start_repair.*
import android.widget.ArrayAdapter
import android.widget.AdapterView.OnItemSelectedListener
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.nikolaenko.andrew.mobileservice.utils.GPSTracker
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.disposables.Disposable
import org.jetbrains.anko.support.v4.toast
import java.io.IOException
import java.util.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import java.io.ByteArrayOutputStream
import android.util.Base64

class FragmentStartRepair : BaseFragment() {

    private var isGranted: Boolean = false
    private var fixType: String? = null
    private var deviceType: String? = null
    private var brand: String? = null
    private var time: String? = null
    private var imageBase64: String? = null
    private var addres: String? = null


    override fun onStart() {
        super.onStart()
        showPermissions()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_start_repair, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        screen.setOnClickListener {
            fixType = screen.text.toString()
            batary.isChecked = false
            diagnostic.isChecked = false
            damage.isChecked = false
            waterDamage.isChecked = false
            repair.isChecked = false
            corpus.isChecked = false
            temperatyre.isChecked = false
            po.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        batary.setOnClickListener {
            fixType = batary.text.toString()
            screen.isChecked = false
            diagnostic.isChecked = false
            damage.isChecked = false
            waterDamage.isChecked = false
            repair.isChecked = false
            corpus.isChecked = false
            temperatyre.isChecked = false
            po.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        diagnostic.setOnClickListener {
            fixType = diagnostic.text.toString()
            screen.isChecked = false
            batary.isChecked = false
            damage.isChecked = false
            waterDamage.isChecked = false
            repair.isChecked = false
            corpus.isChecked = false
            temperatyre.isChecked = false
            po.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        damage.setOnClickListener {
            fixType = damage.text.toString()
            screen.isChecked = false
            batary.isChecked = false
            diagnostic.isChecked = false
            waterDamage.isChecked = false
            repair.isChecked = false
            corpus.isChecked = false
            temperatyre.isChecked = false
            po.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        waterDamage.setOnClickListener {
            fixType = waterDamage.text.toString()
            screen.isChecked = false
            batary.isChecked = false
            diagnostic.isChecked = false
            damage.isChecked = false
            repair.isChecked = false
            corpus.isChecked = false
            temperatyre.isChecked = false
            po.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        repair.setOnClickListener {
            fixType = repair.text.toString()
            screen.isChecked = false
            batary.isChecked = false
            diagnostic.isChecked = false
            damage.isChecked = false
            waterDamage.isChecked = false
            corpus.isChecked = false
            temperatyre.isChecked = false
            po.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        corpus.setOnClickListener {
            fixType = corpus.text.toString()
            screen.isChecked = false
            batary.isChecked = false
            diagnostic.isChecked = false
            damage.isChecked = false
            waterDamage.isChecked = false
            repair.isChecked = false
            temperatyre.isChecked = false
            po.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        temperatyre.setOnClickListener {
            fixType = temperatyre.text.toString()
            screen.isChecked = false
            batary.isChecked = false
            diagnostic.isChecked = false
            damage.isChecked = false
            waterDamage.isChecked = false
            repair.isChecked = false
            corpus.isChecked = false
            po.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        po.setOnClickListener {
            fixType = po.text.toString()
            screen.isChecked = false
            batary.isChecked = false
            diagnostic.isChecked = false
            damage.isChecked = false
            waterDamage.isChecked = false
            repair.isChecked = false
            corpus.isChecked = false
            temperatyre.isChecked = false
            other.isChecked = false
            otherLayout.visibility = View.GONE
        }

        other.setOnClickListener {
            otherLayout.visibility = View.VISIBLE
            screen.isChecked = false
            batary.isChecked = false
            diagnostic.isChecked = false
            damage.isChecked = false
            waterDamage.isChecked = false
            repair.isChecked = false
            corpus.isChecked = false
            temperatyre.isChecked = false
            po.isChecked = false
        }

        editDamage.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                fixType = p0.toString()
            }
        })

        phone.setOnClickListener {
            deviceType = phone.text.toString()
            tablet.isChecked = false
            phoneLayout.visibility = View.VISIBLE
            tabletLayout.visibility = View.GONE
        }
        tablet.setOnClickListener {
            deviceType = tablet.text.toString()
            phone.isChecked = false
            phoneLayout.visibility = View.GONE
            tabletLayout.visibility = View.VISIBLE
        }

        apple.setOnClickListener {
            brand = apple.text.toString()
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        pixel.setOnClickListener {
            brand = pixel.text.toString()
            apple.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        htc.setOnClickListener {
            brand = htc.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        huawei.setOnClickListener {
            brand = huawei.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        lenovo.setOnClickListener {
            brand = lenovo.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        lg.setOnClickListener {
            brand = lg.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        meizu.setOnClickListener {
            brand = meizu.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        motorola.setOnClickListener {
            brand = motorola.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        nokia.setOnClickListener {
            brand = nokia.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        oneplus.setOnClickListener {
            brand = oneplus.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        samsung.setOnClickListener {
            brand = samsung.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            xiaomi.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        xiaomi.setOnClickListener {
            brand = xiaomi.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            other2.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
            modelLayout.visibility = View.GONE
        }
        other2.setOnClickListener {

            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            android.isChecked = false
            windows.isChecked = false
            ios.isChecked = false

            modelLayout.visibility = View.VISIBLE

        }


        editModel.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                brand = p0.toString()
            }
        })

        android.setOnClickListener {
            brand = android.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            windows.isChecked = false
            ios.isChecked = false
        }
        windows.setOnClickListener {
            brand = windows.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            android.isChecked = false
            ios.isChecked = false
        }
        ios.setOnClickListener {
            brand = ios.text.toString()
            apple.isChecked = false
            pixel.isChecked = false
            htc.isChecked = false
            huawei.isChecked = false
            lenovo.isChecked = false
            lg.isChecked = false
            meizu.isChecked = false
            motorola.isChecked = false
            nokia.isChecked = false
            oneplus.isChecked = false
            samsung.isChecked = false
            xiaomi.isChecked = false
            android.isChecked = false
            windows.isChecked = false
        }

        currentLocation.setOnClickListener {
            if (isGranted){
                getCurrentLocation()
            } else {
                showPermissions()
                return@setOnClickListener
            }
            setLocation.isChecked = false
            locationlayout.visibility = View.VISIBLE
        }

        setLocation.setOnClickListener {
            currentLocation.isChecked = false
            address.setText("")
            locationlayout.visibility = View.VISIBLE
        }

        address.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                addres = s.toString().trim()
            }

        })

        allDay.setOnClickListener {
            time = allDay.text.toString()
            morning.isChecked = false
            evening.isChecked = false
        }
        morning.setOnClickListener {
            time = morning.text.toString()
            allDay.isChecked = false
            evening.isChecked = false
        }
        evening.setOnClickListener {
            time = evening.text.toString()
            allDay.isChecked = false
            morning.isChecked = false
        }

        choosePhoto.setOnClickListener {

            if (isGranted){

                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE)

            } else {
                showPermissions()
            }
        }

        tvContinue.setOnClickListener {

            val myId = Prefs.getInt("customer_id", 0)
            val repType = fixType
            val desc = editAboutCompany.text.toString()
            val devType = deviceType
            val brnd = brand
            val addrss = addres
            val photo = imageBase64
            val time = time

            if(myId == 0 || repType == null || desc == null || devType == null || brnd == null || addrss == null || photo == null ||  time == null){
                toast("Не все поля заполнены")
                return@setOnClickListener
            } else {
                replaceFragment(FragmentChooseService.newInstance(myId, repType!!, desc, devType!!, brnd!!, addrss, photo!!, time!!), true)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST_CODE) {

                val selectedImageURI = data?.data

                if (selectedImageURI != null){
                    choosenImage.visibility = View.VISIBLE
                    Glide.with(this).load(selectedImageURI).into(choosenImage)
                    imageBase64 = "data:image/png;base64," + encode(selectedImageURI)

                } else {
                    choosenImage.visibility = View.GONE
                }

            }

        }
    }

    fun encode(imageUri: Uri): String {
        val input = activity?.contentResolver?.openInputStream(imageUri)
        val image = BitmapFactory.decodeStream(input , null, null)
        //encode image to base64 string
        val baos = ByteArrayOutputStream()
        image?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageBytes = baos.toByteArray()
        val imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
        return imageString
    }


    private fun getCurrentLocation(){
        Dexter.withActivity(mainActivity)
                .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        val gps = GPSTracker(context!!)
                        if (gps.canGetLocation()) {

                            Log.e("loca: ", gps.location.toString())

                            val geocoder = Geocoder(context, Locale.getDefault())
                            val addresses = geocoder.getFromLocation(gps.latitude, gps.longitude, 1)
                            val cityName = addresses.get(0).getAddressLine(0)

                            address.setText(cityName)

                            addres = cityName

                        } else {
                            gps.showSettingsAlert()
                        }


                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        token!!.continuePermissionRequest()
                    }

                }).check()
    }

    private fun showPermissions(){

        Dexter.withActivity(mainActivity)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        try {

                            isGranted = report!!.areAllPermissionsGranted()

                        } catch (e: IOException) {
                            toast("Could not create file!")
                        }
                    }
                }).check()
    }

    companion object {
        private val GALLERY_REQUEST_CODE = 100
    }
}