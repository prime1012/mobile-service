package com.nikolaenko.andrew.mobileservice.network

import com.nikolaenko.andrew.mobileservice.model.*
import io.reactivex.Observable
import retrofit2.http.*

interface APIPublicService {

    @FormUrlEncoded
    @POST("/customer/sign-up")
    fun customerSignUp(@FieldMap args: HashMap<String, Any?>?): Observable<CustomerResponse>

    @FormUrlEncoded
    @POST("/customer/sign-in")
    fun customerSignIn(@Field("email") email: String, @Field("password") password: String): Observable<CustomerResponse>

    @GET("/customer/get-info")
    fun getUserInfo(@Query("id") id: Int): Observable<CustomerResponse>

    @FormUrlEncoded
    @POST("/customer/update")
    fun updateUserInfo(@FieldMap args: HashMap<String, Any?>?): Observable<Any>

    @FormUrlEncoded
    @POST("/driver/sign-up")
    fun driverSignUp(@FieldMap args: HashMap<String, Any?>?): Observable<DriverResponse>

    @FormUrlEncoded
    @POST("/driver/sign-in")
    fun driverSignIn(@Field("email") email: String, @Field("password") password: String): Observable<DriverResponse>

    @FormUrlEncoded
    @POST("/service/sign-up")
    fun serviceSignUp(@FieldMap args: HashMap<String, Any?>?): Observable<List<ServiceResponse>>

    @FormUrlEncoded
    @POST("/service/sign-in")
    fun serviceSignIn(@Field("email") email: String, @Field("password") password: String): Observable<ServiceResponse>

    @FormUrlEncoded
    @POST("/order/send")
    fun createOrder(@FieldMap args: HashMap<String, Any?>?): Observable<List<OrderModelNew>>

    @GET("/service/get-all")
    fun getAllServices(): Observable<List<ServicesModel>>

    @GET("/order/get-all-for-customer")
    fun getMyOrders(@Query("id") id: Int): Observable<List<OrderModelNew>>

    @GET("/order/get-all-for-service")
    fun getServiceOrders(@Query("id") id: Int): Observable<List<OrderModelNew>>

    @GET("/service/get-info")
    fun getServiceInfo(@Query("id") id: Int): Observable<ServiceResponse>

    @FormUrlEncoded
    @POST("/service/update")
    fun updateServiceInfo(@FieldMap args: HashMap<String, Any?>?): Observable<Any>

    @GET("/prices/create")
    fun updateCoast(@Query("serviceId") serviceId: Int ,@Query("title") title: String, @Query("price") price: Int): Observable<PriceModel>

    @GET("/prices")
    fun getPriceList(): Observable<List<PriceModel>>

    @FormUrlEncoded
    @POST("/order/accept")
    fun acceptOrder(@Field("id") id: Int): Observable<List<OrderModelNew>>

    @FormUrlEncoded
    @POST("/order/reject")
    fun rejectOrder(@Field("id") id: Int): Observable<List<OrderModelNew>>

    @FormUrlEncoded
    @POST("/order/update")
    fun updateOrder(@FieldMap args: HashMap<String, Any?>?): Observable<List<OrderModelNew>>

    @FormUrlEncoded
    @POST("/order/complete")
    fun completeOrder(@Field("id") id: Int): Observable<List<OrderModelNew>>

    @GET("/driver/get-info")
    fun getDriverInfo(@Query("id") id: Int): Observable<DriverResponse>

    @FormUrlEncoded
    @POST("/driver/update")
    fun updateDriverInfo(@FieldMap args: HashMap<String, Any?>?): Observable<Any>

    @GET("/order/get-all-for-driver")
    fun getDriverOrders(@Query("id") id: Int): Observable<List<OrderModelNew>>
}

