package com.nikolaenko.andrew.mobileservice.network;

import com.google.android.gms.maps.model.PolylineOptions;

public interface DirectionPointListener {
    void onPath(PolylineOptions polyLine);
}
