package com.nikolaenko.andrew.mobileservice.fragment.driver

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.adapter.DriverOrdersAdapter
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.all_orders_driver_fragment.*
import org.jetbrains.anko.support.v4.toast
import java.util.HashMap
import org.json.JSONException
import java.util.Collections.replaceAll
import org.json.JSONObject
import org.json.JSONArray
import android.os.AsyncTask.execute
import com.nikolaenko.andrew.mobileservice.fragment.driver.FragmentDriverMainScreen.GetDisDur





class FragmentDriverMyOrders : BaseFragment() {

    private var infoDisposable: Disposable? = null

    private lateinit var adapter: DriverOrdersAdapter
    private var changes: HashMap<String, Any?>? = null

    override fun onStart() {
        super.onStart()
        changes = HashMap()
        getServiceInfo()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.all_orders_driver_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = DriverOrdersAdapter ({
            id, serviceId, clientId ->
            replaceFragment(FragmentDriverOrderInfo.newInstance(id, serviceId, clientId, true))
        })

        rvPrices.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
        rvPrices.itemAnimator = DefaultItemAnimator()
        rvPrices.adapter = adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }
    }

    private fun getServiceInfo(){

        showProgress(true)

        infoDisposable = apiPulicService.value.getDriverOrders(Prefs.getInt("driver_id", 0))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<OrderModelNew> ->

                    val newList = arrayListOf<OrderModelNew>()
                    newList.clear()

                    for (driverTest in t){

//                        if (driverTest.customerId != 42 && driverTest.serviceId != 42 ){
//                            newList.add(driverTest)
//                        }

                        if (driverTest.customerId != 42 && driverTest.serviceId != 42 && driverTest.address != "ул. Такая-то" && driverTest.status != "complete"){
                            newList.add(driverTest)
                        }
                    }

                    adapter.setList(newList, true)

//                    adapter.setList(t.toMutableList(), true)

                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }
}