package com.nikolaenko.andrew.mobileservice.fragment.driver

import android.annotation.SuppressLint
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.DrawerLayout
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.network.DirectionPointListener
import com.nikolaenko.andrew.mobileservice.network.GeocodingLocation
import com.nikolaenko.andrew.mobileservice.network.GetPathFromLocation
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_home_for_driver.*
import org.jetbrains.anko.support.v4.toast
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*


class FragmentDriverMainScreen : BaseFragment() , GoogleMap.OnMarkerClickListener,  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private var infoDisposable: Disposable? = null
    var mLocationRequest: LocationRequest? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var mLastLocation: Location? = null
    private var googleMap: GoogleMap? = null
    private var locationAddress: GeocodingLocation? = null

    private var dist: String? = null
    internal var markerPoints = arrayListOf<LatLng>()

    override fun onStart() {
        super.onStart()
        locationAddress = GeocodingLocation

        buildGoogleApiClient()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home_for_driver, container, false)
    }

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapView?.onCreate(savedInstanceState)

        mapView?.onResume()

        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mapView?.getMapAsync { mMap ->
            googleMap = mMap

            googleMap?.uiSettings?.isRotateGesturesEnabled = false
            googleMap?.uiSettings?.isCompassEnabled = false
            googleMap?.uiSettings?.isMapToolbarEnabled = false
            googleMap?.uiSettings?.isIndoorLevelPickerEnabled = true
            googleMap?.uiSettings?.isMyLocationButtonEnabled = true

            googleMap?.isMyLocationEnabled = true

            googleMap?.setOnMarkerClickListener(this)

            googleMap?.setOnMapLoadedCallback {

                googleMap?.setOnMarkerClickListener(this)

                val adrOne = Prefs.getString("address_one", null)
                val adrTwo = Prefs.getString("address_two", null)

                if (adrOne != "" && adrOne != "ул. Такая-то" && adrOne != null) {

                    if (adrTwo != "" && adrTwo != "ул. Такая-то" && adrTwo != null) {

                        val oneLatLng = getLocationFromAddress(adrOne)
                        val twoLatLng = getLocationFromAddress(adrTwo)

                        if (oneLatLng != null || twoLatLng != null){

                            markerPoints.add(oneLatLng!!)
                            markerPoints.add(twoLatLng!!)

                            val options1 = MarkerOptions().position(oneLatLng).title("Сервис").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                            val options2 = MarkerOptions().position(twoLatLng).title("Клиент").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            mMap.addMarker(options1)
                            mMap.addMarker(options2)

                            val options = PolylineOptions()
                            options.add(oneLatLng, twoLatLng)
                            options.color(Color.RED)
                            options.width(5f)

                            Handler().postDelayed({

                                GetPathFromLocation(oneLatLng, twoLatLng, DirectionPointListener { polyLine -> googleMap?.addPolyline(polyLine) }).execute()

                                val url = getUrl(oneLatLng, twoLatLng)
                                GetDisDur().execute(url)

                                textDistance.visibility = View.VISIBLE


                            }, 5000)

                        } else {
                            toast("Не удалось проложить маршрут")
                        }


                    }
                } else {
                    textDistance.visibility = View.GONE
                }

            }
        }

        drawer_layout?.addDrawerListener(object : DrawerLayout.DrawerListener{
            override fun onDrawerStateChanged(newState: Int) {

            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

            }

            override fun onDrawerClosed(drawerView: View) {


            }

            override fun onDrawerOpened(drawerView: View) {


            }

        })

        myOrders.setOnClickListener {
            drawer_layout?.closeDrawer(Gravity.LEFT)
            Prefs.remove("address_one")
            Prefs.remove("address_two")
            replaceFragment(FragmentDriverAllOrders(), true)
        }
        allOrders.setOnClickListener {
            drawer_layout?.closeDrawer(Gravity.LEFT)
            Prefs.remove("address_one")
            Prefs.remove("address_two")
            replaceFragment(FragmentDriverMyOrders(), true)
//
        }
        myProfile.setOnClickListener {
            drawer_layout?.closeDrawer(Gravity.LEFT)
            Prefs.remove("address_one")
            Prefs.remove("address_two")
            replaceFragment(FragmentDriverProfile(), true)
        }
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()


        if (mGoogleApiClient != null) {

            if (mGoogleApiClient?.isConnected!!){
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            }

            mGoogleApiClient?.stopAutoManage(mainActivity)
            mGoogleApiClient?.disconnect()
        }


    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()

        if (mGoogleApiClient != null && mGoogleApiClient?.isConnected!!) {
            mGoogleApiClient?.stopAutoManage(mainActivity)
            mGoogleApiClient?.disconnect()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest?.interval = 10000
        mLocationRequest?.fastestInterval = 5000
        mLocationRequest?.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onLocationChanged(p0: Location?) {
        mLastLocation = p0
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(p0!!.latitude, p0.longitude), 16f))
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(context!!)
                .enableAutoManage(mainActivity, 0 /* clientId */, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient?.connect()
    }

    override fun onMarkerClick(p0: Marker?): Boolean {

        return false
    }

    fun getLocationFromAddress(strAddress: String): LatLng? {

        val coder = Geocoder(context, Locale.getDefault())
        val address: List<Address>?
        var p1: LatLng? = null

        try {
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null) {
                return null
            }
            val location = address[0]

            p1 = LatLng(location.latitude, location.longitude)

            return p1
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return p1
    }

    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }



    fun getUrl(origin: LatLng, dest: LatLng): String {

        val str_origin = "origin=" + origin.latitude + "," + origin.longitude
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude
        val mode = "mode=driving"
        val sensor = "sensor=false"
        val parameters = "$str_origin&$str_dest&$sensor&$mode"
        val output = "json"
        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters&key=AIzaSyCqlNJPdnMrSyM2bp1ChLDBvmkNj97xzYQ"
    }


 inner class GetDisDur : AsyncTask<String, String, String>() {

        override fun doInBackground(vararg url: String): String {

            var data = ""

            try {
                var iStream: InputStream? = null
                var urlConnection: HttpURLConnection? = null
                try {
                    val url = URL(url[0])

                    // Creating an http connection to communicate with url
                    urlConnection = url.openConnection() as HttpURLConnection

                    // Connecting to url
                    urlConnection!!.connect()

                    // Reading data from url
                    iStream = urlConnection!!.getInputStream()

                    val br = BufferedReader(InputStreamReader(iStream))

                    val sb = StringBuffer()

                    var line : String?
                    do {

                        line = br.readLine()

                        if (line == null)

                            break

                        sb.append(line)

                    } while (true)


                    data = sb.toString()
                    Log.d("downloadUrl", data)
                    br.close()

                } catch (e: Exception) {
                    Log.d("Exception", e.toString())
                } finally {
                    iStream!!.close()
                    urlConnection!!.disconnect()
                }
            } catch (e: Exception) {
                Log.d("Background Task", e.toString())
            }

            return data
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)

            try {
                val jsonObject = JSONObject(result)

                val routes = jsonObject.getJSONArray("routes")

                val routes1 = routes.getJSONObject(0)

                val legs = routes1.getJSONArray("legs")

                val legs1 = legs.getJSONObject(0)

                val distance = legs1.getJSONObject("distance")

//                val duration = legs1.getJSONObject("duration")

                textDistance.text  = distance.getString("text")
//                durationText = duration.getString("text")

            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }
}