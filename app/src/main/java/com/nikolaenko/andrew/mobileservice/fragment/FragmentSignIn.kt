package com.nikolaenko.andrew.mobileservice.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.client.FragmentHomeScreen
import com.nikolaenko.andrew.mobileservice.fragment.driver.FragmentDriverMainScreen
import com.nikolaenko.andrew.mobileservice.fragment.service.FragmentSeviceScreen
import com.nikolaenko.andrew.mobileservice.model.CustomerResponse
import com.nikolaenko.andrew.mobileservice.model.DriverResponse
import com.nikolaenko.andrew.mobileservice.model.ServiceResponse
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_sign_in.*
import org.jetbrains.anko.support.v4.toast

class FragmentSignIn : BaseFragment() {

    private var userType: Int = 0

    private var email: String = ""
    private var password: String = ""
    private var signInDisposable: Disposable? = null

    override fun onStart() {
        super.onStart()

        userType = arguments?.getInt(ARG_USER_TYPE)!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leftButton.setOnClickListener {
            goBack()
        }

        tvContinue.setOnClickListener {

            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()){

                when(userType){
                    1-> {
                        serviceSignIn(emailEditText.text.toString(), passwordEditText.text.toString())
                    }

                    2-> {
                        driverSignIn(emailEditText.text.toString(), passwordEditText.text.toString())
                    }

                    else -> {
                        clientSignIn(emailEditText.text.toString(), passwordEditText.text.toString())
                    }
                }

            } else {
                toast("Заполните поля!")
                return@setOnClickListener
            }
        }
    }

    private fun clientSignIn(email: String, pass: String) {

        showProgress(true)

        signInDisposable = apiPulicService.value.customerSignIn(email, pass)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: CustomerResponse ->

                    Prefs.putInt("customer_id", t.id!!)
                    mainActivity.hardReplaceFragment(FragmentHomeScreen())
                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    private fun driverSignIn(email: String, pass: String){
        showProgress(true)

        signInDisposable = apiPulicService.value.driverSignIn(email, pass)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: DriverResponse ->

                    Prefs.putInt("driver_id", t.id!!)
                    mainActivity.hardReplaceFragment(FragmentDriverMainScreen())

                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    private fun serviceSignIn(email: String, pass: String){

        showProgress(true)

        signInDisposable = apiPulicService.value.serviceSignIn(email, pass)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: ServiceResponse ->

                    Prefs.putInt("service_id", t.id!!)

                    mainActivity.hardReplaceFragment(FragmentSeviceScreen())
                    showProgress(false)

                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    companion object {

        private val ARG_USER_TYPE = "arg_client"

        fun newInstance(userType: Int): FragmentSignIn {

            val args = Bundle()
            args.putInt(ARG_USER_TYPE, userType)

            val fragment = FragmentSignIn()
            fragment.arguments = args
            return fragment
        }
    }
}
