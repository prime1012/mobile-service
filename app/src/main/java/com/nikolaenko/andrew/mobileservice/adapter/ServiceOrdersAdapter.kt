package com.nikolaenko.andrew.mobileservice.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import com.nikolaenko.andrew.mobileservice.model.PriceModel
import org.jetbrains.anko.find
import org.jetbrains.anko.toast


class ServiceOrdersAdapter(private val callbackReview: ((id: Int, repairType: String, description: String, deviceType: String,
                                                         brand: String, address: String, photo: String, time: String) -> Unit)? = null,
                           private val callbackUpdate: ((id: Int, repairType: String, description: String, deviceType: String,
                                                         brand: String, address: String, photo: String, time: String) -> Unit)? = null) : RecyclerView.Adapter<ServiceOrdersAdapter.MyViewHolder>() {

    private var list: MutableList<OrderModelNew> = ArrayList()

    fun setList(list: MutableList<OrderModelNew>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var repairType: TextView = view.find(R.id.repairType)
        var description: TextView = view.find(R.id.description)
        var item = view
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_for_service_orders, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]

        holder.repairType.text = item.repairType
        holder.description.text = item.description

        holder.item.setOnClickListener {

            if (item.status == "sent"){
                callbackReview?.invoke(item.id!!, item.repairType!!, item.description!!, item.deviceType!!, item.brand!!, item.address!!, item.photo!!, item.shipmentDate!!)
            } else {
                callbackUpdate?.invoke(item.id!!, item.repairType!!, item.description!!, item.deviceType!!, item.brand!!, item.address!!, item.photo!!, item.shipmentDate!!)
            }

        }
    }

    override fun getItemCount(): Int = list.size
}
