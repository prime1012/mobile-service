package com.nikolaenko.andrew.mobileservice.fragment.driver

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.nikolaenko.andrew.mobileservice.R
import com.nikolaenko.andrew.mobileservice.fragment.BaseFragment
import com.nikolaenko.andrew.mobileservice.model.CustomerResponse
import com.nikolaenko.andrew.mobileservice.model.OrderModelNew
import com.nikolaenko.andrew.mobileservice.model.ServiceResponse
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.driver_order_info_fragment.*
import org.jetbrains.anko.support.v4.toast
import java.util.HashMap
import android.location.Geocoder
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.nikolaenko.andrew.mobileservice.network.DirectionPointListener
import com.nikolaenko.andrew.mobileservice.network.GetPathFromLocation
import io.reactivex.rxkotlin.zipWith


class FragmentDriverOrderInfo : BaseFragment() {

    private var infoDisposable: Disposable? = null
    private var id: Int? = null
    private var servId: Int? = null
    private var clinetId: Int? = null
    private var isForMyOrder: Boolean = false

    private var changes: HashMap<String, Any?>? = null

    private var adrOne: String = ""
    private var adrTwo: String = ""

    override fun onStart() {
        super.onStart()

        id = arguments?.getInt(ARG_ID)
        servId = arguments?.getInt(ARG_SERV_ID)
        clinetId = arguments?.getInt(ARG_CLIENT_ID)
        isForMyOrder = arguments?.getBoolean(ARG_FROM_WHERE)!!

        if (isForMyOrder) {
            btnReject.visibility = View.VISIBLE
            btnFinish.visibility = View.VISIBLE
        } else {
            btnAccept.visibility = View.VISIBLE
        }

        toolbarTitle.text = id.toString()

        getServiceInfo()

        changes = HashMap()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.driver_order_info_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnShowOnMap.setOnClickListener {

            Prefs.putString("address_one", adrOne)
            Prefs.putString("address_two", adrTwo)

            mainActivity.hardReplaceFragment(FragmentDriverMainScreen())
        }

        btnAccept.setOnClickListener {

            showProgress(true)

            changes?.put("id", id)
            changes?.put("driverId", Prefs.getInt("driver_id", 0))

            infoDisposable = apiPulicService.value.updateOrder(changes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: List<OrderModelNew> ->

                        showProgress(false)
                        Prefs.putString("address_one", adrOne)
                        Prefs.putString("address_two", adrTwo)
                        mainActivity.hardReplaceFragment(FragmentDriverMainScreen())
                        toast("Вы приняли заказ!")

                    }, { e ->
                        e.printStackTrace()
                        showProgress(false)
                    })
        }

        btnFinish.setOnClickListener {
            showProgress(true)

            infoDisposable = apiPulicService.value.completeOrder(id!!)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: List<OrderModelNew> ->
                        Prefs.remove("address_one")
                        Prefs.remove("address_two")
                        mainActivity.hardReplaceFragment(FragmentDriverMainScreen())
                        toast("Вы завершили заказ!")
                        showProgress(false)
                    }, { e ->
                        toast(e.message.toString())
                        e.printStackTrace()
                        showProgress(false)
                    })



//            changes?.put("id", id)
//            changes?.put("driverId", Prefs.getInt("driver_id", 0))
//            changes?.put("status", "delivered")
//
//            infoDisposable = apiPulicService.value.updateOrder(changes)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe({ t: List<OrderModelNew> ->
//
//
//
//                    }, { e ->
//                        e.printStackTrace()
//                        showProgress(false)
//                    })
        }

        btnReject.setOnClickListener {

            changes?.put("id", id)
            changes?.put("driverId", 0)

            infoDisposable = apiPulicService.value.updateOrder(changes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: List<OrderModelNew> ->

                        Prefs.remove("address_one")
                        Prefs.remove("address_two")

                        mainActivity.hardReplaceFragment(FragmentDriverMainScreen())
                        toast("Вы отменили заказ!")
                        showProgress(false)

                    }, { e ->
                        e.printStackTrace()
                        showProgress(false)
                    })
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mainActivity.setSupportActionBar(toolbar)
        mainActivity.supportActionBar!!.title = null

        leftButton.setOnClickListener {
            goBack()
        }
    }

    private fun getInfo(id: Int, clinetId: Int, drvId: Int){

        infoDisposable = apiPulicService.value.getDriverOrders(drvId)
                .zipWith(apiPulicService.value.getUserInfo(clinetId)) { t1, t2 ->
                    Pair(t1, t2)
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: Pair<List<OrderModelNew>, CustomerResponse> ->

                    for (order in t.first){

                        if (order.id == id ){
                            clientTime.text = order.shipmentDate
                            clientAddress.text = order.address
                            adrTwo = order.address!!
                            serviceTime.text = order.shipmentDate
                        }
                    }


                    clientName.text = t.second.name + " " + t.second.surname
                    clientPhone.text = t.second.phone

                    showProgress(false)

                }, { e ->
                    e.printStackTrace()
                })

    }

    private fun getServiceInfo(){

        showProgress(true)

        apiPulicService.value.getServiceInfo(servId!!)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: ServiceResponse ->

                    serviceTitle.text = t.title
                    serviceAddress.text = t.address

                    adrOne = t.address!!

                    servicePhone.text = t.phone

                    if (isForMyOrder){
                        getInfo(id!!, clinetId!!, Prefs.getInt("driver_id", 0))
                    } else {
                        getInfo(id!!, clinetId!!, 0)
                    }



                }, { e ->
                    toast(e.message.toString())
                    e.printStackTrace()
                    showProgress(false)
                })
    }

    companion object {

        private val ARG_ID = "arg_user_id"
        private val ARG_SERV_ID = "arg_service_id"
        private val ARG_CLIENT_ID = "arg_client_id"
        private val ARG_FROM_WHERE = "arg_from_where"

        fun newInstance(id: Int, servId: Int, clientId: Int, isForMyOrder: Boolean): FragmentDriverOrderInfo {

            val args = Bundle()
            args.putInt(ARG_ID, id)
            args.putInt(ARG_SERV_ID, servId)
            args.putInt(ARG_CLIENT_ID, clientId)
            args.putBoolean(ARG_FROM_WHERE, isForMyOrder)

            val fragment = FragmentDriverOrderInfo()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onStop() {
        super.onStop()
        infoDisposable?.dispose()
    }
}